import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { CaptureComponent } from './capture/capture.component';
import { PickStyleComponent } from './pick-style/pick-style.component';
import { ResultComponent } from './result/result.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'capture', component: CaptureComponent },
  { path: 'pickStyle', component: PickStyleComponent },
  { path: 'result', component: ResultComponent },
  { path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
