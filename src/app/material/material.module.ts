import { NgModule } from '@angular/core';
import { MatToolbarModule, MatButtonModule, MatCardModule } from '@angular/material'

const mods = [MatToolbarModule, MatButtonModule, MatCardModule];

@NgModule({
  declarations: [],
  imports: mods,
  exports: mods,
})
export class MaterialModule { }
