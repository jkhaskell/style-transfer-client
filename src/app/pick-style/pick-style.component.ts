import { Component, OnInit } from '@angular/core';
import { ImageService } from '../image.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pick-style',
  templateUrl: './pick-style.component.html',
  styleUrls: ['./pick-style.component.scss']
})
export class PickStyleComponent implements OnInit {

  styles = [
    {name: 'Candy', image: 'assets/img/candy.jpg'},
    {name: 'Udnie', image: 'assets/img/udnie.jpg'},
    {name: 'Mosaic', image: 'assets/img/mosaic.jpg'},
    {name: 'Rain', image: 'assets/img/rain-princess.jpg'},
  ];

  constructor(private imageService: ImageService, private router: Router) { }

  ngOnInit() {
  }

  selectStyle(style) {
    this.imageService.style = style.name;
    this.router.navigateByUrl('/result');
  }

}
