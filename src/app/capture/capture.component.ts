import { Component, OnInit } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { WebcamImage } from 'ngx-webcam';
import { ImageService } from '../image.service';

@Component({
  selector: 'app-capture',
  templateUrl: './capture.component.html',
  styleUrls: ['./capture.component.scss']
})
export class CaptureComponent implements OnInit {

  seconds: number;
  trigger: Subject<void> = new Subject<void>();

  constructor(private imageService: ImageService) { }

  ngOnInit() {
  }

  triggerSnapshot(): void {
    this.seconds = 3;
    this.trigger.next();
    this.seconds = null;
  }

  handleImage(webcamImage: WebcamImage): void {
    console.log('received webcam image', webcamImage);
    this.imageService.webcamImage = webcamImage;
    console.log(this.imageService.webcamImage);
  }

  get triggerObservable(): Observable<void> {
    return this.trigger.asObservable();
  }

  retake() {
    this.imageService.webcamImage = null;
  }

}
